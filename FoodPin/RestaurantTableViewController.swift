//
//  RestaurantTableViewController.swift
//  FoodPin
//
//  Created by fred fu on 2020/9/4.
//  Copyright © 2020 fred fu. All rights reserved.
//

import UIKit

class RestaurantTableViewController: UITableViewController {
    private let reuseIdentifier = "Cell"

    var restaurantNames = [
        "Cafe Deadend",
        "Homei",
        "Teakha",
        "Cafe Loisl",
        "Petite Oyster",
        "For Kee Restaurant",
        "Po's Atelier",
        "Bourke Street Bakery",
        "Haigh's Chocolate",
        "Palomino Espresso",
        "Upstate",
        "Traif",
        "Graham Avenue Meats And Deil",
        "Waffle & Wolf",
        "Five Leaves",
        "Cafe Lore",
        "Confessional",
        "Barrafina",
        "Donostia",
        "Royal Oak",
        "CASK Pub and Kitchen"
    ]
}

extension RestaurantTableViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        restaurantNames.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "datacell", for: indexPath)
        cell.textLabel?.text = restaurantNames[indexPath.row]
        return cell
    }
}
